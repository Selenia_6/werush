import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GamesModel} from '../../models/games.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {
  public searchString: string;
  games$: Observable<GamesModel>;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.games$ = this.http.get<GamesModel>(
      'https://bitbucket.org/Selenia_6/werush/raw/aec5ee619bb28befaf82a807c80e5520630a0b68/src/assets/MOCK_DATA.json');
  }
}
