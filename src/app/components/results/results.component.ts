import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})

export class ResultsComponent {
  public games: any;
  @Input() gamesList: any;
  @Input() searchString: string;
}
